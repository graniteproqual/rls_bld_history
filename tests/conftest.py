import json

"""conftest.py"""


# @pytest.fixture
def test_config():
    """default relative location of data folder"""
    config_dict = {
        "compare_data_folder": "tests/compare_data/",
        "other_config_stuff_key": "other_config_stuff_value"
    }
    return({"compare_data_folder": "tests/compare_data/"})


# @pytest.fixture
def json_are_equivalent(json1, json2):
    """check if 2 json fragments are equivalent"""
    return(json.dumps(json1, sort_keys=True) == json.dumps(json2, sort_keys=True))
