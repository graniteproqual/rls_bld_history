# from glob import glob
import os
import re

"""get list of test results files"""
commit_test_session_results = [x for x in os.listdir("public") if re.match(".*.report.html$", x)]
"""...and sort list because we're only going to report 10 most recent"""
commit_test_session_results.sort(reverse=True)
commit_test_session_results = commit_test_session_results[:10]


# commit_test_session_results = commit_test_session_results.sort()
# del commit_test_session_results[10:]
# print("commit_test_session_results: ", end="")
# print(commit_test_session_results)
# print(len(commit_test_session_results))
# del commit_test_session_results[3:]
# print("commit_test_session_results: ", end="")
# print(commit_test_session_results)
# print(len(commit_test_session_results))

# regular expression for line that will contain all of the result summaries
# 'expected failures' seems to be a text string unique in the document for this line
re_for_summary_numbers_line = "^.*expected failures.*$"


# these are specific regular expressions for parts of re_for_summary_numbers_line to extract
#             <span class="passed">11 passed</span>
re_passed = r'<span class="passed">\d* passed</span>'
re_skipped = r'<span class="skipped">\d* skipped</span>'
re_failed = r'<span class="failed">\d* failed</span>'
re_error = r'<span class="error">\d* errors</span>'
re_xfailed = r'<span class="xfailed">\d* expected failures</span>'
re_xpassed = r'<span class="xpassed">\d* unexpected passes</span>'

re_results_list = [
    re_passed,
    re_skipped,
    re_failed,
    re_error,
    re_xfailed,
    re_xpassed,
]

all_sessions_results = {}  # to accumulate results for all session, test_session_id is key
with open("templates/results-base-template.html", "r") as rbtfile:
    with open("public/index.html", "w") as indexfile:
        for rbtline in rbtfile:
            if re.search("<!--place holder for generated content-->", rbtline):
                indexfile.write("<ul>")
                for f in commit_test_session_results:
                    test_session_id = f[0:19]  # e.g. first 19 chars of filename: (2021-08-18T18:33:00).report.html
                    with open("public/" + f, 'r') as infile:
                        for line in infile:
                            results_line_match = re.match(re_for_summary_numbers_line, line)
                            if results_line_match is not None:
                                indexfile.write("<li>")
                                results_line = results_line_match.group()
                                results_to_display = \
                                    '<a href = "' + test_session_id + '.report.html">' + \
                                    test_session_id + '</a>&nbsp;&nbsp;&nbsp;'
                                for regex in re_results_list:
                                    results_match = re.search(regex, results_line)
                                    if results_match is not None:
                                        results_to_display += results_match.group() + ",  "
                                results_to_display = results_to_display[:-3]
                                indexfile.write(results_to_display)
                                indexfile.write("</li>")
                indexfile.write("/<ul>")
            indexfile.write(rbtline)
#

# re_list = {
#     "passed": "passed",
#     "skipped": "skipped",
#     "failed": "failed",
#     "error": "errors",
#     "xfailed": "expected failures",
#     "xpassed": "unexpected passes",
# }

"""
found_list = []
for


re.findall()

class_s = r'['
info_s = r'['
for key in re_list:
    class_s += key + '|'
    info_s += re_list[key] + '|'
class_s = class_s[:-1] + ']'
info_s = info_s[:-1] + ']'

lineregex = r'^.*<p\\sclass\\s=\\s"filter".*$'

results_classes = r'[passed|skipped|failed|error|xfailed|xpassed]'
# regex for something like this: <span class="passed">20 passed</span>
regex = \
    r'<span class="' + class_s + r'">\\d*.\b' + info_s + r'</span>'

regex = "^.*expected failures.*$"
print("__________________________________________")
print(regex)
print('<span class="xfailed">0 expected failures</span>')
print('<span class="error">0 errors</span>')

results_line_regex = \
    '^.*<.+p.+class.+=.+"filter"'
print("KEYS")
for f in commit_test_session_results:
    print(f)
    key = f[0:19]

    print("key " + key)
    with open("public/" + f, 'r') as infile:
        for line in infile:
            results_line = re.match(regex, line)
            if results_line is not None:
                print(line + " found")
                print(results_line)

# < span class = "passed" > 11 passed < /span >,
"""
"""
< span class = "passed" > 11 passed < /span > ,
< span class = "passed" > 11 passed < /span >,
< span class = "skipped" > 2 skipped < /span >,
< span class = "failed" > 0 failed < /span >,
< span class = "error" > 0 errors < /span >,
< span class = "xfailed" > 0 expected failures < /span >,
< span class = "xpassed" > 0 unexpected passes < /span >
doc, tag, text = Doc().tagtext()

with tag('html'):
    with tag('body'):
        with tag('header', id='main'):
            text('Last few Commit-Tests results')
        with tag('a', href='/my-url'):
            text('some link')

result = doc.getvalue()
"""
